//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ___VARIABLE_sceneName___Interactor: ___VARIABLE_sceneName___BusinessLogic, ___VARIABLE_sceneName___DataStore {
    private var presenter: ___VARIABLE_sceneName___PresentationLogic
    private var worker: ___VARIABLE_sceneName___Worker?
    //var name: String = ""
    
    init(presenter: ___VARIABLE_sceneName___PresentationLogic) {
        self.presenter = presenter
    }
    
    // MARK: Do something
    
    func doSomething(request: ___VARIABLE_sceneName___.Something.Request) {
        worker = ___VARIABLE_sceneName___Worker()
        worker?.doSomeWork()
        
        let response = ___VARIABLE_sceneName___.Something.Response()
        presenter.presentSomething(response: response)
    }
}
